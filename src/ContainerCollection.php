<?php


namespace Eiprice\Webdriver;

use \ArrayIterator;

use Eiprice\Core\Contract\ISpiderContainer;

class ContainerCollection extends ArrayIterator
{
    /**
     * ContainerCollection constructor.
     * @param Shipment ...$shipments
     */
    public function __construct(ISpiderContainer ...$containers)
    {
        parent::__construct($containers);
    }

    /**
     * @return Shipment
     */
    public function current() : ISpiderContainer
    {
        return parent::current();
    }

    /**
     * @param $offset
     * @return Shipment
     */
    public function offsetGet($offset) : ISpiderContainer
    {
        return parent::offsetGet($offset);
    }

    /**
     * @param Shipment $shipment
     */
    public function add(ISpiderContainer $container) : void
    {
        $this->getInnerIterator()->append($container);
    }

    /**
     * @param int $key
     * @param Shipment $shipment
     */
    public function set(int $key, ISpiderContainer $container) : void
    {
        $this->getInnerIterator()->offsetSet($key, $container);
    }

    /**
     * @return Shipment
     */
    public function next() : ?ISpiderContainer
    {
        return parent::next();
    }
}
