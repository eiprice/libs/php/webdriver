<?php


namespace Eiprice\Webdriver\Contract;


use Eiprice\Webdriver\ContainerCollection;
use Eiprice\Webdriver\Exceptions\ConnectionTimeoutException;
use Eiprice\Webdriver\Exceptions\RequestErrorException;
use Eiprice\Webdriver\Exceptions\ServerErrorException;
use Eiprice\Webdriver\Exceptions\UserRequestErrorException;
use Eiprice\Core\Contract\ISpiderContainer;
use Eiprice\Webdriver\RequestCollection;

/**
 * Interface IWebdriver
 * @package Eiprice\Webdriver\Contract
 */
interface IWebdriver
{
    /**
     * Configura a URL que será executado a requisição
     *
     * @param string $url
     */
    public function setUrl(string $url) : void;

    /**
     * @param string $method
     */
    public function setMethod(string $method) : void;

    /**
     * @param $name
     * @param $value
     * @param $domain
     */
    public function addCookie($name, $value, $domain) : void ;


    /**
     * @return string|null
     */
    public function lastUrl() : ?string;


    /**
     * @return array
     */
    public function getHistory() : array;


    /**
     * Executa a requisição Web
     *
     * @throws ConnectionTimeoutException
     * @throws RequestErrorException
     * @throws ServerErrorException
     * @throws UserRequestErrorException
     */
    public function execute($headers = [], $payload = []) : void;

    /**
     * Retorna o conteudo dentro de um container
     *
     * @return ISpiderContainer
     */
    public function get_container() : ?ISpiderContainer;

    /**
     * @param $key
     * @param $value
     */
    public function addHeader($key, $value) : void;

    /**
     * @param array $header
     */
    public function setHeader(array $header) : void;

    /**
     * @param RequestCollection $collection
     */
    public function bulkRequest(RequestCollection $collection) : ContainerCollection;
}
