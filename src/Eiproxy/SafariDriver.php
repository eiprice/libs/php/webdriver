<?php

namespace Eiprice\Webdriver\Eiproxy;



use Eiprice\Webdriver\EiproxyDriver;
use Psr\Log\LoggerInterface;

class SafariDriver extends EiproxyDriver
{
    public function __construct($service_url, LoggerInterface $logger, $user_agent = 'Eiproxy')
    {

        parent::__construct($service_url, $logger, $user_agent);
    }
}
