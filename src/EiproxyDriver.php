<?php


namespace Eiprice\Webdriver;

use Eiprice\Core\Contract\ISpiderContainer;
use Eiprice\Core\Container\SpiderContainer;
use Eiprice\Webdriver\Contract\IWebdriver;
use Eiprice\Webdriver\Exceptions\ConnectionTimeoutException;
use Eiprice\Webdriver\Exceptions\RequestErrorException;
use Eiprice\Webdriver\Exceptions\ServerErrorException;
use Eiprice\Webdriver\Exceptions\UserRequestErrorException;
use Eiprice\Webdriver\Traits\Base;
use Psr\Log\LoggerInterface;

/**
 * Class ScraperApi
 * @package Eiprice\Webdriver
 */
class EiproxyDriver implements IWebdriver
{
    use Base;

    /**
     * @var string
     */
    protected $service_url;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var ISpiderContainer
     */
    protected $container;



    protected $guzzleDriver;


    public function __construct($service_url, LoggerInterface $logger, $user_agent = 'Eiproxy')
    {
        $this->service_url = $service_url;

        $this->logger = $logger;

        $this->guzzleDriver = new GuzzleDriver($this->logger);
    }

    public function addCookie($name, $value, $domain) : void
    {
        $this->guzzleDriver->addCookie($name, $value, $domain);
    }

    /**
     * @return string
     */
    protected function getProxy() : string
    {
        $headers = $this->guzzleDriver->get_container()->get_headers();
        $proxy = 'None';

        if ( isset($headers['proxy'])){
            $proxy = end($headers['proxy']);
            $proxy = json_decode($proxy, true);
            $proxy = explode("@", $proxy['http'])[1];
            $this->logger->debug('Proxy', [$proxy]);   
        }

        return $proxy;
    }

    /**
     *
     *
     * @throws ConnectionTimeoutException
     * @throws RequestErrorException
     * @throws ServerErrorException
     * @throws UserRequestErrorException
     */
    public function execute($headers = [], $payload = []): void
    {

        $this->addHeader("Url", $this->url);

        $ei_headers = $this->headers;

        if ( ! isset($ei_headers['headers']['Referer'])){
            $ei_headers['headers']['Referer'] = $this->lastUrl();
        }

        $this->logger->debug("Sendind headers", $ei_headers);

        $this->guzzleDriver->setHeader($ei_headers);


        $this->guzzleDriver->setMethod($this->method);
        $this->guzzleDriver->setUrl($this->service_url);

        $this->guzzleDriver->execute($headers, $payload);

        $this->guzzleDriver->get_container()->set_proxy($this->getProxy());
        $this->guzzleDriver->get_container()->setUrl($this->url);

        $this->addHistory($this->url);
    }

    /**
     * @return ISpiderContainer
     */
    public function get_container(): ?ISpiderContainer
    {
        return $this->guzzleDriver->get_container();
    }

}

