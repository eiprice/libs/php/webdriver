<?php


namespace Eiprice\Webdriver\Exceptions;

use \Exception;

/**
 * Class ConnectionTimeoutException
 * @package Eiprice\Webdriver\Exceptions
 */
class ConnectionTimeoutException extends BaseException
{

}
