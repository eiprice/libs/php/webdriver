<?php


namespace Eiprice\Webdriver\Exceptions;

use \Exception;

/**
 * Class ServerErrorException
 * @package Eiprice\Webdriver\Exceptions
 */
class ServerErrorException extends BaseException
{

}
