<?php


namespace Eiprice\Webdriver\Guzzle;


use Psr\Log\LoggerInterface;
use Eiprice\Webdriver\GuzzleDriver;

/**
 * Class ChromeDriver
 * @package System\Drivers\Webdrivers\Guzzle
 */
class ChromeDriver extends GuzzleDriver
{
    function __construct(LoggerInterface $logger)
    {
        parent::__construct(
            $logger,
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36'
        );
    }
}
