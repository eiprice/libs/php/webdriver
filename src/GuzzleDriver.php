<?php


namespace Eiprice\Webdriver;

use Eiprice\Core\Contract\ISpiderContainer;
use Eiprice\Core\Container\SpiderContainer;

use Eiprice\Webdriver\Traits\Base;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SessionCookieJar;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;
use Eiprice\Webdriver\Contract\IWebdriver;
use Eiprice\Webdriver\Exceptions\ConnectionTimeoutException;
use Eiprice\Webdriver\Exceptions\RequestErrorException;
use Eiprice\Webdriver\Exceptions\ServerErrorException;
use Eiprice\Webdriver\Exceptions\UserRequestErrorException;
use GuzzleHttp\Cookie\SetCookie;


/**
 * Class GuzzleDriver
 * @package Eiprice\Webdriver
 */
class GuzzleDriver implements IWebdriver
{
    use Base;

    /**
     * @var string
     */
    protected $user_agent = '';

    /**
     * @var string
     */
    protected $url;


    protected $ref = null;
    protected $param = [];

    /**
     * @var GuzzleClient
     */
    protected $client;

    /**
     * @var ISpiderContainer
     */
    protected $container;

    /**
     * @var SessionCookieJar
     */
    protected $cookies;

    /**
     * @var string $user_agent
     * @var LoggerInterface $logger
     */
    protected $logger;

    /**
     * GuzzleDriver constructor.
     * @param LoggerInterface|null $logger
     * @param string $user_agent
     */
    function __construct(LoggerInterface $logger = null, $user_agent = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:64.0) Gecko/20100101 Firefox/64.0')
    {
        ///
        $this->user_agent = $user_agent;

        // Grava os cookies na SESSAO
        $this->cookies = new SessionCookieJar('PHPSESSID', true);

        //
        $this->logger = $logger;
    }

    /**
     * @param $name
     * @param $value
     * @param $domain
     */
    public function addCookie($name, $value, $domain) : void
    {
        $cookie = new SetCookie();
        $cookie->setName($name);
        $cookie->setValue($value);
        $cookie->setDomain($domain);

        $this->cookies->setCookie($cookie);

        $this->logger->debug("Adding cookie", [$cookie]);
        $this->logger->debug("After cookie", $this->cookies->toArray());

        $this->cookies->save();
    }


    /**
     *
     * @return GuzzleClient
     */
    protected function getCliente() : GuzzleClient
    {
        if ( empty($this->client)){
            $this->client = new GuzzleClient([
                'headers' => [
                    'User-Agent' => $this->user_agent
                ],
                'verify' => false,
                'cookies' => $this->cookies,
                'connect_timeout' => 30,
                'timeout' => 60
            ]);
        }

        return $this->client;
    }

    /**
     * @return array
     */
    protected function getParams($headers, $payload) : array
    {
        $host = $this->getHost();

        $param = array_replace_recursive(
            [
                'timeout' => 6000,
                'connect_timeout' => 6000,
                'verify' => false,
                'headers' => ['Host' => $host]
            ], $this->param
        );


        $param['headers'] = array_merge($this->headers, $param['headers']);
        $param['headers'] = array_merge($headers, $param['headers']);

        if ( ! isset($param['headers']['Referer'])){
            $param['headers']['Referer'] = $this->lastUrl();
        }

        if ( !empty($payload['json'])){
            $param['json'] = $payload['json'];
        }

        if ( !empty($payload['form_params'])){
            $param['form_params'] = $payload['form_params'];
        }

        return $param;
    }


    public function execute($headers = [], $payload = []) : void
    {
        try{
            $this->logger->info("Fetching URL: {$this->url}");



            $params = $this->getParams($headers, $payload);

            $this->wait();

            $this->logger->debug("Cookie", $this->cookies->toArray());
            $this->logger->info("{$this->method} {$this->url}", $params);
            $res = $this->getCliente()->request($this->method, $this->url, $params);
            $code = $res->getStatusCode();


            if ( $code >= 200 && $code <= 299 ){
                $body_class = get_class($res->getBody());
                $this->logger->debug("Class of the content {$body_class}");
                $this->logger->debug("Sent headers", $res->getHeaders());
                $container = new SpiderContainer();
                $container->setContent((string)$res->getBody());
                $container->setHeaders($res->getHeaders());
                $container->setUrl($this->url);
                $this->container = $container;
            } elseif (  $code >= 400 && $code <= 499)   {
                throw new UserRequestErrorException("Request Error. Code: {$code}");
            } elseif (  $code >= 500 && $code <= 599)   {
                throw new ServerErrorException("Server Error. Code: {$code}");
            } elseif ( $code == 0) {
                throw new ConnectionTimeoutException("Timeoout");
            } else {
                throw new RequestErrorException("Request Error. Code: {$code}");
            }
        } catch (GuzzleException $e){
            $this->logger->error("Request Error", ["code" => $e->getCode(), 'message' => $e->getMessage(), 'line' => $e->getLine()]);
            throw new RequestErrorException("Request Error", $e->getCode());
        }

        $this->addHistory($this->url);
    }

    /**
     * @return ISpiderContainer
     */
    public function get_container() : ?ISpiderContainer
    {
        return $this->container;
    }
}
