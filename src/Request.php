<?php


namespace Eiprice\Webdriver;


use Eiprice\Webdriver\Contract\IWebdriver;

class Request
{
    protected $method;
    protected $url;
    protected $headers = [];
    protected $payload = [];
    /**
     * @var callable|null
     */
    protected $action = null;

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param mixed $method
     */
    public function setMethod($method): void
    {
        $this->method = $method;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param array $headers
     */
    public function setHeaders(array $headers): void
    {
        $this->headers = $headers;
    }

    /**
     * @return array
     */
    public function getPayload(): array
    {
        return $this->payload;
    }

    /**
     * @param array $payload
     */
    public function setPayload(array $payload): void
    {
        $this->payload = $payload;
    }

    public function executeAction(IWebdriver $webdriver) : void
    {
        ($this->action)($webdriver);
    }

    /**
     * @return bool
     */
    public function hasAction() : bool
    {
        return is_callable($this->action);
    }

    /**
     * @param callable $action
     */
    public function setAction(callable $action) : void
    {
        $this->action = $action;
    }
}
