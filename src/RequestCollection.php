<?php


namespace Eiprice\Webdriver;

use \ArrayIterator;

/**
 * Class RequestCollection
 * @package Eiprice\Webdriver
 */
class RequestCollection extends ArrayIterator
{
    /**
     * RequestCollection constructor.
     * @param Request ...$requests
     */
    public function __construct(Request ...$requests)
    {
        parent::__construct($requests);
    }

    /**
     *
     * @param array $requests
     * @return RequestCollection
     */
    public static function from_array(array $requests) : self
    {
        $collection = new self();
        foreach ($requests as $data_request){
            $request = new Request();
            $request->setMethod($data_request['method']);
            $request->setUrl($data_request['url']);
            $request->setHeaders($data_request['headers']);
            $request->setAction(isset($data_request['action']) ? $data_request['action'] : null);
            $request->setPayload(isset($data_request['payload']) ? $data_request['payload'] : []);

            $collection[] = $request;
        }

        return $collection;
    }

    /**
     * @return Shipment
     */
    public function current() : Request
    {
        return parent::current();
    }

    /**
     * @param $offset
     * @return Shipment
     */
    public function offsetGet($offset) : Request
    {
        return parent::offsetGet($offset);
    }

    /**
     * @param Shipment $request
     */
    public function add(Request $request) : void
    {
        $this->getInnerIterator()->append($request);
    }

    /**
     * @param int $key
     * @param Shipment $request
     */
    public function set(int $key, Request $request) : void
    {
        $this->getInnerIterator()->offsetSet($key, $request);
    }

    /**
     * @return Shipment
     */
    public function next() : ?Request
    {
        return parent::next();
    }
}
