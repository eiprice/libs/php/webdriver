<?php


namespace Eiprice\Webdriver;

use Eiprice\Core\Contract\ISpiderContainer;
use Eiprice\Core\SpiderContainer;
use Eiprice\Webdriver\Contract\IWebdriver;
use Eiprice\Webdriver\Exceptions\ConnectionTimeoutException;
use Eiprice\Webdriver\Exceptions\RequestErrorException;
use Eiprice\Webdriver\Exceptions\ServerErrorException;
use Eiprice\Webdriver\Exceptions\UserRequestErrorException;

/**
 * Class ScraperApi
 * @package Eiprice\Webdriver
 */
class ScraperApiDriver implements IWebdriver
{
    /**
     * @var string
     */
    protected $api_key;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var ISpiderContainer
     */
    protected $container;

    function __construct($api_key = '', LoggerInterface $logger = null)
    {
        $this->api_key = $api_key;

        $this->logger = $logger;
    }


    public function setUrl(string $url) : void
    {
        if ( empty($url)){
            throw new \Exception("URL cannot be empty");
        }
        $this->url = $url;
    }

    public function exectute(): void
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,
            "http://api.scraperapi.com/?api_key={$this->api_key}&url={$this->url}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Accept: application/json"
        ));

        $response = curl_exec($ch);
        curl_close($ch);

        $container = new SpiderContainer();
        $container->setContent($response);
        $this->container = $container;
    }

    public function get_container(): ISpiderContainer
    {
        
    }

}
