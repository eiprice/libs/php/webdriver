<?php


namespace Eiprice\Webdriver\Traits;

use Eiprice\Webdriver\ContainerCollection;
use Eiprice\Webdriver\Exceptions\ConnectionTimeoutException;
use Eiprice\Webdriver\Exceptions\EmptyURL;
use Eiprice\Webdriver\Exceptions\RequestErrorException;
use Eiprice\Webdriver\Exceptions\ServerErrorException;
use Eiprice\Webdriver\Exceptions\UserRequestErrorException;
use Eiprice\Webdriver\Request;
use Eiprice\Webdriver\RequestCollection;

/**
 * Trait Base
 * @package Eiprice\Webdriver\Traits
 */
trait Base
{
    /**
     *
     *
     * @var string $method
     */
    protected $method = 'GET';

    /**
     *
     *
     * @var array $headers
     */
    protected $headers = [];

    /**
     *
     *
     * @var array $history
     */
    protected $history = [];

    /**
     * @var int
     */
    protected $sleep = 3;


    /**
     *
     *
     * @param string $url
     * @throws EmptyURL
     */
    public function setUrl(string $url) : void
    {
        if ( empty($url)){
            throw new EmptyURL("URL cannot be empty");
        }
        $this->url = $url;
    }

    /**
     *
     *
     * @param array $array
     */
    public function setHeader( array $array) : void
    {
        $this->headers = $array;
    }


    /**
     *
     *
     * @param $key
     * @param $value
     */
    public function addHeader($key, $value) : void
    {
        $this->headers[$key] = $value;
    }


    /**
     *
     *
     * @param $method
     */
    public function setMethod($method) : void
    {
        $this->method = $method;
    }

    /**
     *
     *
     * @return string|null
     */
    public function lastUrl() : ?string
    {
        return  count($this->history) > 0 ? end($this->history) : null;
    }

    /**
     *
     *
     * @return array
     */
    public function getHistory() : array
    {
        return $this->history;
    }

    /**
     *
     *
     * @param $url
     */
    protected function addHistory($url) : void
    {
        $this->history[] = $url;
    }

    /**
     * @return string
     */
    protected function getHost() : string
    {
        preg_match('@(http[s]?:\/\/)?(.*?)\/@is', $this->url, $match);
        return $match[2];
    }

    protected function setSleep($sleep)
    {
        $this->sleep = $sleep;
    }

    /**
     *
     */
    protected function wait()
    {
        sleep($this->sleep);
    }

    /**
     *
     * @param RequestCollection $collection
     * @return ContainerCollection
     * @throws ConnectionTimeoutException
     * @throws Exceptions\EmptyURL
     * @throws RequestErrorException
     * @throws ServerErrorException
     * @throws UserRequestErrorException
     */
    public function bulkRequest(RequestCollection $collection) : ContainerCollection
    {
        $responseCollection = new ContainerCollection();

        foreach ($collection as $request){
            /**
             * @var Request $request
             */
            $url = $request->getUrl();
            if (is_callable($url)){
                $url = $url();
            }

            $this->setUrl($url);
            $this->setMethod($request->getMethod());
            $this->execute($request->getHeaders(), $request->getPayload());


            $responseCollection[] = $this->get_container();

            if ( $request->hasAction()){
                $request->executeAction($this);
            }
        }

        return $responseCollection;
    }
}
